<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Person;
class PersonController extends Controller
{
    //

    public function index(){
        $per = Person::all();
        return view('admin/person/index')->with('person',$per);
    }
}
